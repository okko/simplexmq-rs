# simplexmq-rs

A rust implementation

Yes it will be blazingly fast.

No it is not ready yet

# Docs

All work is based off of these [docs](https://github.com/simplex-chat/simplexmq/blob/stable/protocol/simplex-messaging.md)

# Licence

[AGPL-3.0-only](./LICENCE)
