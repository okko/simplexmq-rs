use std::net::IpAddr;

// move to somewhere
#[derive(Default)]
enum SignAlrogithm {
    #[default]
    ED448,
    ED25519,
}

// Options for server
struct InitOptions {
    enable_store_log: bool,
    log_stats: bool,
    sign_alrogithm: SignAlrogithm,
    ip: IpAddr, // should accept maybe also https://...
    fqdn: Option<IpAddr>,
    password: Option<String>, // idk
    scripted: bool,
}
